function vault_login {
  METADATA_URI="http://169.254.169.254/metadata"
  RESOURCE_URI="https://management.azure.com"
  API_VERSION="2018-02-01"

  ACCESS_TOKEN=$(curl -s -H "Metadata: true" "$METADATA_URI/identity/oauth2/token?api-version=$API_VERSION&resource=$RESOURCE_URI&object_id=$MSI_OBJ_ID" | jq -r '.access_token')

  if [ -z "${ACCESS_TOKEN}" ] || [ "${ACCESS_TOKEN}" = "null" ]; then
    echo "Couldn't retrieve access token, exiting.."
    exit 1
  fi

  COMPUTE_DATA=$(curl -s -H Metadata:true "$METADATA_URI/instance?api-version=$API_VERSION")
  VM_NAME=$(echo $COMPUTE_DATA|jq -r .compute.name)
  VM_RESOURCE_GROUP=$(echo $COMPUTE_DATA|jq -r .compute.resourceGroupName)
  VM_SUBSCRIPTION_ID=$(echo $COMPUTE_DATA|jq -r .compute.subscriptionId)

  export VAULT_TOKEN=$(vault write -format=json auth/azure/login \
    jwt=$ACCESS_TOKEN \
    role=$CICD_VAULT_AZURE_AUTH_ROLE \
    subscription_id=$VM_SUBSCRIPTION_ID \
    resource_group_name=$VM_RESOURCE_GROUP \
    vm_name=$VM_NAME|jq -r .auth.client_token)
}
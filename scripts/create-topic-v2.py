import yaml, json, os
import subprocess

clustername = os.getenv('CLUSTERNAME')
filename = os.getenv('FILE')

def get_msvc(arg):
    global filename
    cut_string = clustername.split("-")
    print("Cluster name      :", clustername)
    if(cut_string[0] == "shared"):
     msvc = cut_string[1]
     print("Microservice Name :", msvc)
    else:
     msvc = cut_string[0]
     print("Microservice Name :", msvc)
    env = clustername.split("-")[-1]
    print("Environtment      :", env)
    clusterfilename = clustername.replace("-","_")
    # filename = "services/{}/ansible/inventory/".format(msvc) + "{}/group_vars/".format(env) + "metadata_kafka_cluster_kafka_{}.yaml".format(clusterfilename)
    print("Metadata Location :", filename)
    return msvc,env,clusterfilename,filename

def get_zookeeperhost(arg):
    global zookeeper_host
    global data
    with open(filename) as stream:
        data = yaml.load(stream ,Loader=yaml.BaseLoader)
        zookeeper_host = data['zookeeper_connect']
    return zookeeper_host

def get_existing_topic():
    global exists_topics
    global need_to_create_topic
    bashCMD=["scripts/kafka/bin/kafka-topics.sh","--list","--zookeeper", "{}".format(zookeeper_host)]
    process = subprocess.Popen(bashCMD, stdout=subprocess.PIPE)
    output, error = process.communicate()
    exists_topics = output.decode("utf-8").splitlines()
    print("Existing Topic   :", exists_topics)
    need_to_create_topic = []
    for d in data['kafka_topic_config']:
        if d['topic'] not in exists_topics:
            need_to_create_topic.append(d['topic'])
    print("New Topic    :", need_to_create_topic)

def new_topic():
    for i in data['kafka_topic_config']:
        if i['topic'] in need_to_create_topic:
            TOPIC_NAME = i['topic']
            REPLICATION_FACTOR = i['replication_number']
            PARTITION_NUMBER = i['partition_number']
            bashCMD=["scripts/kafka/bin/kafka-topics.sh","--zookeeper", "{}".format(zookeeper_host),"--create","--topic","{}".format(TOPIC_NAME), "--replication-factor", "{}".format(REPLICATION_FACTOR), "--partitions", "{}".format(PARTITION_NUMBER)]
            process = subprocess.Popen(bashCMD, stdout=subprocess.PIPE)
            output, error = process.communicate()
            try:
                CONFIG = ','.join(i['config'])
                bashCMD=["scripts/kafka/bin/kafka-topics.sh","--zookeeper", "{}".format(zookeeper_host),"--alter","--entity-type","topics","--entity-name","{}".format(TOPIC_NAME),"--add-config", "{}".format(CONFIG)]
                process = subprocess.Popen(bashCMD, stdout=subprocess.PIPE)
                output, error = process.communicate()
            except:
                pass

def add_topic_config():
    for i in data['kafka_topic_config']:
        if i['topic'] in need_to_create_topic:
            TOPIC_NAME = i['topic']
            CONFIG = ','.join(i['config'])
            bashCMD=["scripts/kafka/bin/kafka-topics.sh","--zookeeper", "{}".format(zookeeper_host),"--alter","--entity-type","topics","--entity-name","{}".format(TOPIC_NAME),"--add-config", "{}".format(CONFIG)]
            process = subprocess.Popen(bashCMD, stdout=subprocess.PIPE)
            output, error = process.communicate()

def current_topic():
    bashCMD=["scripts/kafka/bin/kafka-topics.sh", "--describe", "--zookeeper", "{}".format(zookeeper_host), "--topic", "."]
    process = subprocess.Popen(bashCMD, stdout=subprocess.PIPE)
    output, error = process.communicate()
    print("Current Topic    : ", output.decode("utf-8").splitlines())

##### run the function #####
print("====================================================")
print("                   Creating Topic                   ")
print("====================================================")
get_msvc(clustername)
get_zookeeperhost(filename)
get_existing_topic()
new_topic()
# current_topic()
# add_topic_config()

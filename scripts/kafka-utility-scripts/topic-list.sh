#!/bin/bash
set -e

list_exist_topic(){
    echo "--------------------------------------------------"
    echo " LIST EXISTING TOPIC (UNIQUE) "
    echo "--------------------------------------------------"
    echo " Zookeeper : $ZOOKEEPER_SERVER"
    echo " Date/Time : `date`"
    echo "--------------------------------------------------"
    kafka/bin/kafka-topics.sh --zookeeper $ZOOKEEPER_SERVER --list > topic-unsorted.txt

    ### Sort & Remove Duplicate ###
    cat -n topic-unsorted.txt | sort -uk2 | sort -nk1 | cut -f2- > topics.txt

    ### Show All Topics ###
    cat topics.txt

    echo ""
    echo "--------------------------------------------------"
    echo " Finish At : `date`"
    echo "--------------------------------------------------"
    echo "--- SUCCESS ALL DONE --- "
    echo ""
}

main(){
    export ZOOKEEPER_SERVER="$1:2181"
    list_exist_topic $ZOOKEEPER_SERVER
}

### START HERE ###
main $1


### How Execute Bash ####
# ./topic-list.sh [zookeeper]
# -----
# ./topic-list.sh zookeeper_host

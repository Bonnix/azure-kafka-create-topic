import requests
import argparse
import json
import time

exclude_status = ['RUNNING', 'PAUSED']

def getArg():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--server', required=True)
    parser.add_argument('-m', '--mode', default='status')
    args = parser.parse_args()
    return args

def getConnectors(server):
    req_path = 'http://%s/connectors' % (server)
    con_req = requests.get(req_path)
    json_response = con_req.json()
    return json_response

def checkConnectorStatus(server, connector):
    req_path = 'http://%s/connectors/%s/status' % (server, connector)
    con_req = requests.get(req_path)
    json_response = con_req.json()
    return json_response

def pauseConnector(server, connector):
    url = 'http://%s/connectors/%s/pause' % (server, connector)
    req = requests.put(url)
    return req

def resumeConnector(server, connector):
    url = 'http://%s/connectors/%s/resume' % (server, connector)
    req = requests.put(url)
    return req

def main():
    args = getArg()
    connectors = getConnectors(args.server)
    connectors.sort()
    counter = 0
    for connector in connectors:
        if args.mode == 'pause':
            print('pause : %s' % (connector))
            con_req = pauseConnector(args.server, connector)
            print(con_req)
        elif args.mode == 'resume':
            counter += 1
            print('resuming : %s' % (connector))
            con_req = resumeConnector(args.server, connector)
            print(con_req)
            if counter == 50:
                print('sleeping 5 seconds')
                time.sleep(5)
                counter = 0
        elif args.mode == 'status':
            con_status = checkConnectorStatus(args.server, connector)
            print('connector-status;%s;%s;%s' % (connector, con_status['connector']['worker_id'], con_status['connector']['state']))
            tasks = con_status['tasks']
            for task in tasks:
                # if task['state'] not in exclude_status:
                print('task-status;%s;%s;%s' % (connector, task['worker_id'], task['state']))
if __name__ == '__main__':
    main()


#!/bin/bash
set -e

load_config(){
    echo "--------------------------------------------------"
    echo " Config's File  : $CONFIG_FILE"
    echo "--------------------------------------------------"
    kafka_configs=`cat $CONFIG_FILE`   # configs.txt
}

load_file(){
    echo "--------------------------------------------------"
    echo " Topic's File  : $TOPIC_FILE"
    echo "--------------------------------------------------"
    kafka_topics=`cat $TOPIC_FILE`     # topics.txt
}

execute_setup_config(){
    for config_topic in $kafka_configs
    do
        for topic in $kafka_topics
        do
            echo "--------------------------------------------------"
            echo " REMOVE OVERRIDES CONFIG TOPIC "
            echo "--------------------------------------------------"
            echo " Config's Name : $config_topic"
            echo " Topic's Name  : $topic"
            echo " Date/Time     : `date`"
            echo "--------------------------------------------------"
            bin/kafka-topics.sh --zookeeper $ZOOKEEPER_SERVER --alter --topic $topic --delete-config $config_topic
            echo "--------------------------------------------------"
            echo " --- DONE ---"
            echo ""
            sleep 3
        done
    done
    echo ""
    echo "--------------------------------------------------"
    echo " Finish At : `date`"
    echo "--------------------------------------------------"
    echo "--- SUCCESS ALL DONE --- "
    echo ""
}

main(){
    # -------------------------------------------
    #  ZOOKEEPER_SERVER="172.21.128.85:2181/kafka"
    # -------------------------------------------
    export ZOOKEEPER_SERVER="$1:2181"
    export CONFIG_FILE=$2
    export TOPIC_FILE=$3
    load_config $CONFIG_FILE
    load_file $TOPIC_FILE
    execute_setup_config $ZOOKEEPER_SERVER $kafka_configs $kafka_topics
}

### START HERE ###
main $1 $2 $3

### How Execute Bash ####
# ./topic-config-remove.sh [zookeper] [config_file] [topics_file]
# -----
# ./topic-config-remove.sh zookeeper_host configs.txt topics.txt

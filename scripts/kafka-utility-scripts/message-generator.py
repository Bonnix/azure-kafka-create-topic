from kafka import KafkaProducer
from kafka.errors import KafkaError
import hashlib
import json
import random
import time
import argparse

parser = argparse.ArgumentParser(description='Finding Latest Offset Timestamp')
parser.add_argument('-b', '--broker', help='broker ip:port', required=True)
parser.add_argument('-t', '--topic', help='topic name', required=True)

args = parser.parse_args()
topic = args.topic
broker = args.broker

producer = KafkaProducer(bootstrap_servers=[broker], value_serializer=lambda m: json.dumps(m).encode('ascii'))
msg_list = ['burton', 'cliff', 'rando', 'harry', 'slacky', 'betty', 'aroon']

while True:
    i = 0
    while i < 100:
        i += 1
        hash = hashlib.sha1()
        hash.update(str(time.time()).encode('utf-8'))
        key = hash.hexdigest()[:10]
        value = random.choice(msg_list)
        future = producer.send(topic, { key: value })
        try:
            record_metadata = future.get(timeout=5)
        except KafkaError:
            log.exception()
            pass
    print('%d messages has been sent' % (i))
    time.sleep(5)

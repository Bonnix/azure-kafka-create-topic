#!/bin/bash
set -e

list_exist_group(){
    echo "--------------------------------------------------"
    echo " LIST EXISTING GROUP (UNIQUE) "
    echo "--------------------------------------------------"
    echo " Broker    : $BROKER_SERVER"
    echo " Date/Time : `date`"
    echo "--------------------------------------------------"
    kafka/bin/kafka-consumer-groups.sh --bootstrap-server $BROKER_SERVER --list > group-unsorted.txt

    ### Sort & Remove Duplicate ###
    cat -n group-unsorted.txt | sort -uk2 | sort -nk1 | cut -f2- > groups.txt

    ### Show All Groups ###
    cat groups.txt

    echo ""
    echo "--------------------------------------------------"
    echo " Finish At : `date`"
    echo "--------------------------------------------------"
    echo "--- SUCCESS ALL DONE --- "
    echo ""
}

main(){
    export BROKER_SERVER="$1:9092"
    list_exist_group $BROKER_SERVER
}

### START HERE ###
main $1


### How Execute Bash ####
# ./group-list.sh [broker]
# -----
# ./group-list.sh broker_host

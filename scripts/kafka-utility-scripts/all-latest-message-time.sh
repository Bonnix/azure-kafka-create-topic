#!/bin/bash

input=$2
while IFS= read -r line
do
  if [[ "$line" != "" ]]
    then
       python latest-message.py -b $1:9092 -t $line | tail -1
    else
       echo "no-data, no-data"
  fi
done < "$input"

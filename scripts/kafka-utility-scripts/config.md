## Customize Config for Topic

### List Configs
* cleanup.policy
  ```
  If set to compact , the messages in this topic will be discarded such that only the most recent message with a given key is retained (log compacted).
  ```

* compression.type
  ```
  The compression type used by the broker when writing message batches for this topic to disk. Current values are gzip, snappy and lz4.
  ```

* delete.retention.ms
  ```
  How long, in milliseconds, deleted tombstones will be retained for this topic. Only valid for log compacted topics.
  ```

* file.delete.delay.ms
  ```
  How long, in milliseconds, to wait before deleting log segments and indices for this topic from disk.
  ```

* flush.messages
  ```
  How many messages are received before forcing a flush of this topic’s messages to disk.
  ```

* flush.ms
  ```
  How long, in milliseconds, before forcing a flush of this topic’s messages to disk.
  ```

* index.interval.bytes
  ```
  How many bytes of messages can be produced between entries in the log segment’s index.
  ```

* max.message.bytes
  ```
  The maximum size of a single message for this topic, in bytes.
  ```

* message.format.version
  ```
  The message format version that the broker will use when writing messages to disk.
  Must be a valid API version number (e.g., “0.10.0”).
  ```

* message.timestamp.difference.max.ms
  ```
  The maximum allowed difference, in milliseconds, between the message timestamp and the broker timestamp when the message is received.
  This is only valid if the message.timestamp.type is set to CreateTime .
  ```

* message.timestamp.type
  ```
  Which timestamp to use when writing messages to disk. Current values are CreateTime for the timestamp specified by the client and LogAppendTime for the time when the message is written to the partition by the broker.
  ```

* min.cleanable.dirty.ratio
  ```
  How frequently the log compactor will attempt to compact partitions for this topic, expressed as a ratio of the number of uncompacted log segments to the total number of log segments. Only valid for log compacted topics.
  ```

* min.insync.replicas
  ```
  The minimum number of replicas that must be in-sync for a partition of the topic to be considered available.
  ```

* preallocate
  ```
  If set to true , log segments for this topic should be preallocated when a new segment is rolled.
  ```

* retention.bytes
  ```
  The amount of messages, in bytes, to retain for this topic.
  ```

* retention.ms
  ```
  How long messages should be retained for this topic, in milliseconds.
  ```

* segment.bytes
  ```
  The amount of messages, in bytes, that should be written to a single log segment in a partition.
  ```

* segment.index.bytes
  ```
  The maximum size, in bytes, of a single log segment index.
  ```

* segment.jitter.ms
  ```
  A maximum number of milliseconds that is randomized and added to segment.ms when rolling log segments.
  ```

* segment.ms
  ```
  How frequently, in milliseconds, the log segment for each partition should be rotated.
  ```

* unclean.leader.election.enable
  ```
  If set to false , unclean leader elections will not be permitted for this topic.
  ```

### Overriding Topic Configuration Defaults
```
kafka-configs.sh --zookeeper [zk_host]:2181/[namespace] --alter --entity-type topics --entity-name [topic_name] --add-config <key>=<value>[,<key>=<value>...]
```

### Remove Topic Configuration Overrides
```
kafka-configs.sh --zookeeper [zk_host]:2181/[namespace] --alter --entity-type topics --entity-name [topic_name] --delete-config [config_name]
```

#!/bin/bash
set -e

load_file(){
    echo "--------------------------------------------------"
    echo " Topic's File  : $TOPIC_FILE"
    echo "--------------------------------------------------"
    kafka_topics=`cat $TOPIC_FILE`     # topics.txt
}

show_topic_detail(){
    for topic in $kafka_topics
    do
        echo "--------------------------------------------------"
        echo " DESCRIBE TOPIC (SHOW DETAIL) "
        echo "--------------------------------------------------"
        echo " Topic's Name : $topic"
        echo " Date/Time    : `date`"
        echo "--------------------------------------------------"
        kafka/bin/kafka-topics.sh --zookeeper $ZOOKEEPER_SERVER --describe --topic $topic
        echo "--------------------------------------------------"
        echo "--- DONE ---"
        echo ""
        sleep 2
    done
    echo ""
    echo "--------------------------------------------------"
    echo " Finish At : `date`"
    echo "--------------------------------------------------"
    echo "--- SUCCESS ALL DONE --- "
    echo ""
}

main(){
    # -------------------------------------------
    #  ZOOKEEPER_SERVER="172.21.128.85:2181/kafka"
    # -------------------------------------------
    export ZOOKEEPER_SERVER="$1:2181"
    export TOPIC_FILE=$2
    load_file $TOPIC_FILE
    show_topic_detail $BROKER_SERVER $kafka_topics
}

### START HERE ###
main $1 $2


### How Execute Bash ####
# ./topic-detail.sh [zookeeper] [topics_file]
# -----
# ./topic-detail.sh zookeeper_host topics.txt

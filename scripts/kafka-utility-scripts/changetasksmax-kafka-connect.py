import requests
import argparse
import json
import time

def getArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--server', required=True)
    parser.add_argument('-c', '--connector', required=True)
    parser.add_argument('-t', '--tasksmax', required=True, type=int)
    parser.add_argument('-r', '--resume')
    parser.add_argument('-p', '--pause')
    args = parser.parse_args()
    return args

def getConfig(server, connector):
    url = 'http://%s/connectors/%s/config' % (server, connector)
    req = requests.get(url)
    return req

def putConfig(server, connector, payload):
    url = 'http://%s/connectors/%s/config' % (server, connector)
    headers = {'Content-Type': 'application/json'}
    data_payload = json.dumps(payload)
    req = requests.put(url, data=data_payload, headers=headers)
    return req

def resumeConnector(server, connector):
    url = 'http://%s/connectors/%s/resume' % (server, connector)
    req = requests.put(url)
    return req

def pauseConnector(server, connector):
    url = 'http://%s/connectors/%s/pause' % (server, connector)
    req = requests.put(url)
    return req

def statusCon(server, connector):
    url = 'http://%s/connectors/%s/status' % (server, connector)
    req = requests.get(url)
    return req

def main():
    args = getArgs()
    connector = args.connector
    server = args.server
    tasksmax = args.tasksmax

    if args.resume:
        resume_con = resumeConnector(server, connector)
        print('resuming connector')
        print(resume_con.status_code)

    if args.pause:
        pause_con = pauseConnector(server, connector)
        print('pausing connector')
        print(pause_con.status_code)

    config_req = getConfig(server, connector)
    if config_req.status_code == 200:
        config_json = config_req.json()
        # print('current config')
        # print(config_json)
        config_json['tasks.max'] = tasksmax
        print('changing config')
        put_conf = putConfig(server, connector, config_json)
        if put_conf.status_code == 200:
            print('config changed')
        new_config = getConfig(server, connector)
        # print(new_config.json())
    else:
        print(config_req.status_code)

    print('sleeping 5 seconds')
    time.sleep(5)
    status_con = statusCon(server, connector)
    print(json.dumps(status_con.json(), indent=4, sort_keys=True))

if __name__ == '__main__':
    main()

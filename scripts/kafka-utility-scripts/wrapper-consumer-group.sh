#!/usr/bin/env bash
bootstrap_server=$1
consumer_group_list=$2
while IFS= read -r line
do
	./consumer-group-lag.sh ${bootstrap_server} "${line}"
done < "${consumer_group_list}"

#!/bin/bash
TOTAL_LAG=0
BROKER="$1:9092"
CONSUMER_GROUP="$2"
DESCRIBE_FILE="./describe.txt"
LAGS_FILE="./lags.txt"

kafka/bin/kafka-consumer-groups.sh --bootstrap-server $BROKER --group "$CONSUMER_GROUP" --describe | tail -n+3 > ${DESCRIBE_FILE}

# echo "broker: ${BROKER}, consumer group: ${CONSUMER_GROUP}"

while read DETAILS
do
	LAG=$(echo $DETAILS | awk '{print $6;}')
	TOPIC=$(echo $DETAILS | awk '{print $2;}')
	TOTAL_LAG=$((TOTAL_LAG+LAG))
done < ${DESCRIBE_FILE}

# printf "${CONSUMER_GROUP},${TOTAL_LAG}\n" >> ${LAGS_FILE}
echo "${CONSUMER_GROUP}, ${TOPIC}, ${TOTAL_LAG}"

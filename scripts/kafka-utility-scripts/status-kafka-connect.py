import requests
import argparse
import json

exclude_status = ['RUNNING', 'PAUSED']

def getArg():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--server', required=True)
    args = parser.parse_args()
    return args

def getConnectors(server):
    req_path = 'http://%s/connectors' % (server)
    con_req = requests.get(req_path)
    json_response = con_req.json()
    return json_response

def checkConnectorStatus(server, connector):
    req_path = 'http://%s/connectors/%s/status' % (server, connector)
    con_req = requests.get(req_path)
    json_response = con_req.json()
    return json_response

def main():
    args = getArg()
    connectors = getConnectors(args.server)
    connectors.sort()
    for connector in connectors:
        con_status = checkConnectorStatus(args.server, connector)
        print('connector-status;%s;%s;%s' % (connector, con_status['connector']['worker_id'], con_status['connector']['state']))
        tasks = con_status['tasks']
        for task in tasks:
            # if task['state'] not in exclude_status:
            print('task-status;%s;%s;%s' % (connector, task['worker_id'], task['state']))
if __name__ == '__main__':
    main()

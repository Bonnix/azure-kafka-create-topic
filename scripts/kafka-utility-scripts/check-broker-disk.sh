#!/bin/bash
set -e

load_file(){
    echo "--------------------------------------------------"
    echo " Topic's File  : $TOPIC_FILE"
    echo "--------------------------------------------------"
    kafka_topics=`cat $TOPIC_FILE`     # topics.txt
}

execute_check(){
    mkdir -p $KAFKA_RETENTION

    for topic in $kafka_topics
    do
        echo "--------------------------------------------------"
        echo " SHOW DISK USAGE EACH TOPIC "
        echo "--------------------------------------------------"
        echo " Topic's Name : $topic"
        echo " Date/Time    : `date`"
        echo "--------------------------------------------------"
        find /data/ -name "$topic*" | xargs -I {} du -h --max-depth=1 {} | sort -k 1 > $KAFKA_RETENTION/disk-usage-$topic.txt

        filename="$KAFKA_RETENTION/disk-usage-$topic.txt"

        echo "Save to: $filename"
        cat $filename
        min=$(cat $filename | head -n 1 | awk '{print $1}');
        max=$(cat $filename | tail -n 1 | awk '{print $1}');
        echo "--------------------------------------------------"
        echo "Min-Max: $min - $max"
        echo "--- DONE --- "
        sleep 1
    done

    echo ""
    echo "--------------------------------------------------"
    echo " Finish At : `date`"
    echo "--------------------------------------------------"
    echo "--- SUCCESS ALL DONE --- "
    echo ""
}

main(){
    export KAFKA_RETENTION="/tmp/kafka-retention"
    export TOPIC_FILE=$1
    load_file $TOPIC_FILE
    execute_check $kafka_topics
}

### START HERE ###
main $1


### How Execute Bash ####
# ./check-broker-disk.sh [topics_file]
# -----
# ./check-broker-disk.sh topics.txt

#!/bin/bash
set -e

load_file(){
    echo "--------------------------------------------------"
    echo " Group's File  : $GROUP_FILE"
    echo "--------------------------------------------------"
    kafka_groups=`cat $GROUP_FILE`     # groups.txt
}

execute_delete(){
    for group in $kafka_groups
    do
        echo "--------------------------------------------------"
        echo " DELETE CONSUMER GROUPS "
        echo "--------------------------------------------------"
        echo " Group's Name : $group"
        echo " Date/Time    : `date`"
        echo "--------------------------------------------------"
        kafka/bin/kafka-consumer-groups.sh --zookeeper $ZOOKEEPER_SERVER --delete --group $group
        echo "--------------------------------------------------"
        echo "--- DONE ---"
        echo ""
        sleep 2
    done
    echo ""
    echo "--------------------------------------------------"
    echo " Finish At : `date`"
    echo "--------------------------------------------------"
    echo "--- SUCCESS ALL DONE --- "
    echo ""
}

main(){
    # -------------------------------------------
    #  ZOOKEEPER_SERVER="172.21.128.85:2181/kafka"
    # -------------------------------------------
    export ZOOKEEPER_SERVER="$1:2181"
    export GROUP_FILE=$2
    load_file $GROUP_FILE
    execute_delete $ZOOKEEPER_SERVER $kafka_topics
}

### START HERE ###
main $1 $2


### How Execute Bash ####
# ./delete-consumer-group.sh [zookeper] [groups_file]
# -----
# ./delete-consumer-group.sh zookeeper_host groups.txt

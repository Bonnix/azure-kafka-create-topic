from kafka import KafkaConsumer, TopicPartition
import argparse
import datetime
import time
import csv

groupID = 'pythoncg'
SLEEP = 2

def get_arg():
    parser = argparse.ArgumentParser(description='Finding Latest Offset Timestamp')
    parser.add_argument('-b', '--broker', help='broker ip:port', required=True)
    parser.add_argument('-t', '--topic', help='topic name')
    parser.add_argument('-o', '--output', help='output result to file')
    args = parser.parse_args()
    return args

def get_latest_timestamp(consumer, topic):
    partitions = consumer.partitions_for_topic(topic)
    int_timestamp = 0
    print(partitions)
    for partition in partitions:
        topic_partition = TopicPartition(topic=topic, partition=partition)
        end_offset = consumer.end_offsets([topic_partition])[topic_partition]
        if end_offset > 0:
            consumer.assign([topic_partition])
            consumer.seek(topic_partition, end_offset - 1)
            message = consumer.poll(5000, 10)
            if len(message) > 0:
                msgs = message[topic_partition]
                if len(msgs) > 0:
                    if int_timestamp < msgs[-1].timestamp:
                        int_timestamp = msgs[-1].timestamp
    
    if int_timestamp > 0:
        dt_last = datetime.datetime.fromtimestamp(int_timestamp / 1000)
        dt_last_str = dt_last.strftime("%Y-%m-%d %H:%M:%S")
        return '%s, %s' % (topic, dt_last_str)
    else:
        return '%s, no-data' % (topic)

def main():
    args = get_arg()
    consumer = KafkaConsumer(bootstrap_servers=[args.broker], max_poll_records=500)
    if args.topic is not None:
        latest_message_date = get_latest_timestamp(consumer, args.topic)
        print(latest_message_date)
    else:
        topics = consumer.topics()
        for topic in topics:
            print(topic)
            latest_message_date = get_latest_timestamp(consumer, topic)
            print(latest_message_date)
            if args.output is not None:
                with open(args.output, 'a+', newline='') as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerow(latest_message_date.split(','))
            time.sleep(SLEEP)
    consumer.close()
if __name__ == '__main__':
    main()


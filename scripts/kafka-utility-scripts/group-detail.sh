#!/bin/bash
set -e

load_file(){
    echo "--------------------------------------------------"
    echo " Group's File  : $GROUP_FILE"
    echo "--------------------------------------------------"
    kafka_groups=`cat $GROUP_FILE`     # groups.txt
}

show_group_detail(){
    for group in $kafka_groups
    do
        echo "--------------------------------------------------"
        echo " DESCRIBE GROUP (SHOW DETAIL) "
        echo "--------------------------------------------------"
        echo " Group's Name : $group"
        echo " Date/Time    : `date`"
        echo "--------------------------------------------------"
        kafka/bin/kafka-consumer-groups.sh --bootstrap-server $BROKER_SERVER --describe --group $group
        echo "--------------------------------------------------"
        echo "--- DONE ---"
        echo ""
        sleep 2
    done
    echo ""
    echo "--------------------------------------------------"
    echo " Finish At : `date`"
    echo "--------------------------------------------------"
    echo "--- SUCCESS ALL DONE --- "
    echo ""
}

main(){
    export BROKER_SERVER="$1:9092"
    export GROUP_FILE=$2
    load_file $GROUP_FILE
    show_group_detail $BROKER_SERVER $kafka_groups
}

### START HERE ###
main $1 $2


### How Execute Bash ####
# ./group-detail.sh [broker] [groups_file]
# -----
# ./group-detail.sh broker_host groups.txt

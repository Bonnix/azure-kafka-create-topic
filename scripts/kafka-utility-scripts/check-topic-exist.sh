#!/bin/bash
set -e

load_file(){
    echo "--------------------------------------------------"
    echo " Topic's File  : $TOPIC_FILE"
    echo "--------------------------------------------------"
    kafka_topics=`cat $TOPIC_FILE`     # topics.txt
}

execute_check(){
    touch topic-unsorted.txt

    for topic in $kafka_topics
    do
        echo "--------------------------------------------------"
        echo " VALIDATE TOPIC EXISTING "
        echo "--------------------------------------------------"
        echo " Topic's Name : $topic"
        echo " Date/Time    : `date`"
        echo "--------------------------------------------------"
        kafka/bin/kafka-topics.sh --zookeeper $ZOOKEEPER_SERVER --list | grep $topic | awk '{print $1;}' >> topic-unsorted.txt
        echo "--------------------------------------------------"
        echo " --- DONE ---"
    done
}

show_topics_sorted(){
    echo "--------------------------------------------------"
    echo " Exist Topics (Unique)"
    echo "--------------------------------------------------"
    ### Sort & Remove Duplicate ###
    cat -n topic-unsorted.txt | sort -uk2 | sort -nk1 | cut -f2- > topic-sorted.txt

    ### Show All Topics ###
    cat topic-sorted.txt

    echo ""
    echo "--------------------------------------------------"
    echo " Finish At : `date`"
    echo "--------------------------------------------------"
    echo "--- SUCCESS ALL DONE --- "
    echo ""
}

main(){
    # -------------------------------------------
    #  ZOOKEEPER_SERVER="172.21.128.85:2181/kafka"
    # -------------------------------------------
    export ZOOKEEPER_SERVER="$1:2181"
    export TOPIC_FILE=$2
    load_file $TOPIC_FILE
    execute_check $ZOOKEEPER_SERVER $kafka_topics
    show_topics_sorted
}

### START HERE ###
main $1 $2


### How Execute Bash ####
# ./check-topic.sh [zookeper] [topics_file]
# -----
# ./check-topic.sh zookeeper_host topics.txt

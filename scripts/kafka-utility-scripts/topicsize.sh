#!/usr/bin/env bash
bootstrap_server=$1
topic=$2
while IFS= read -r line
do
 ./consumer-group-lag.sh ${bootstrap_server} "${line}"
  kafka/bin/kafka-log-dirs.sh --topic-list $line --bootstrap-server ${bootstrap_server}:9092 --describe | grep '^{'   | jq '[ ..|.size? | numbers ] | add'
done < "${topic}"

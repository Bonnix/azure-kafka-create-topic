#!/bin/bash
set -e

load_file(){
    echo "--------------------------------------------------"
    echo " Topic's File  : $TOPIC_FILE"
    echo "--------------------------------------------------"
    kafka_topics=`cat $TOPIC_FILE`     # topics.txt
}

execute_create(){
    # partition=$(awk -F_ '{print $2}' <<< $2)

    for topic in $kafka_topics
    do
        echo "--------------------------------------------------"
        echo " CREATE NEW TOPICS "
        echo "--------------------------------------------------"
        echo " Topic's Name : $topic"
        echo " Begin At     : `date`"
        echo "--------------------------------------------------"
        kafka/bin/kafka-topics.sh --zookeeper $ZOOKEEPER_SERVER --create --topic $topic --replication-factor $REPLICATION_FACTOR --partitions $NUMBER_PARTITION
        echo "--------------------------------------------------"
        echo "--- DONE ---"
        echo ""
        sleep 2
    done
    echo ""
    echo "--------------------------------------------------"
    echo " Finish At : `date`"
    echo "--------------------------------------------------"
    echo "--- SUCCESS ALL DONE --- "
    echo ""
}

main(){
    # -------------------------------------------
    #  ZOOKEEPER_SERVER="172.21.128.85:2181/kafka"
    # -------------------------------------------
    export ZOOKEEPER_SERVER="$1:2181"
    export TOPIC_FILE=$2
    export REPLICATION_FACTOR=$3
    export NUMBER_PARTITION=$4
    load_file $TOPIC_FILE
    execute_create $ZOOKEEPER_SERVER $kafka_topics $REPLICATION_FACTOR $NUMBER_PARTITION
}

### START HERE ###
main $1 $2 $3 $4


### How Execute Bash ####
# ./create-topic.sh [zookeper] [topics_file] [replication_factor] [number_partition]
# -----
# ./create-topic.sh zookeeper_host 12_partition.txt 3 12
# ./create-topic.sh zookeeper_host 18_partition.txt 3 18
# ./create-topic.sh zookeeper_host 27_partition.txt 3 27
# ./create-topic.sh zookeeper_host 30_partition.txt 3 30
# ./create-topic.sh zookeeper_host 60_partition.txt 3 60

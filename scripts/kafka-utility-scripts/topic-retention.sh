#!/bin/bash
set -e

load_file(){
    echo "--------------------------------------------------"
    echo " Topic's File  : $TOPIC_FILE"
    echo "--------------------------------------------------"
    kafka_topics=`cat $TOPIC_FILE`     # topics.txt
}

execute_retention(){
    for topic in $kafka_topics
    do
        echo "--------------------------------------------------"
        echo " SET TOPIC RETENTION (ms) "
        echo "--------------------------------------------------"
        echo " Topic's Name : $topic"
        echo " Date/Time    : `date`"
        echo "--------------------------------------------------"
        kafka/bin/kafka-topics.sh --zookeeper $ZOOKEEPER_SERVER --alter --topic $topic --config retention.ms=$TOPIC_RETENTION
        echo "--------------------------------------------------"
        echo " --- DONE ---"
        echo ""
        sleep 10
    done
    echo ""
    echo "--------------------------------------------------"
    echo " Finish At : `date`"
    echo "--------------------------------------------------"
    echo "--- SUCCESS ALL DONE --- "
    echo ""
}

main(){
    # -------------------------------------------
    #  ZOOKEEPER_SERVER="172.21.128.85:2181/kafka"
    # -------------------------------------------
    export ZOOKEEPER_SERVER="$1:2181"
    export TOPIC_FILE=$2
    export TOPIC_RETENTION=$3
    load_file $TOPIC_FILE
    execute_retention $ZOOKEEPER_SERVER $kafka_topics $TOPIC_RETENTION
}

### START HERE ###
main $1 $2 $3

### How Execute Bash ####
# ./topic-retention.sh [zookeper] [topics_file] [topics_retention_ms]
# -----
# ./topic-retention.sh zookeeper_host topics.txt 604800000
#
# -----------
# days-to-ms
# -----------
# 30 days    = 2592000000
# 15 days    = 1296000000
# 10 days    = 864000000
#  7 days    = 604800000
#  5 days    = 432000000
#  3 days    = 259200000
#  1 day     = 86400000
# 12 hours   = 43200000
#  6 hours   = 21600000
#  3 hours   = 10800000
#  1 hour    = 3600000
# 30 minutes = 1800000
# 15 minutes = 900000
# 10 minutes = 600000
#  5 minutes = 300000
#  1 minute  = 60000

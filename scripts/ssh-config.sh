#!/bin/bash
filename=$1
function append_prep()
{
  echo "### AZURE VM" >> ~/.ssh/config
  echo "Host 172.*" >> ~/.ssh/config
  echo "  User $BASTION_USER@azure.bukalapak.io" >> ~/.ssh/config
  echo "  CertificateFile ~/.ssh/id_rsa.pub-aadcert.pub" >> ~/.ssh/config
  echo "  UserKnownHostsFile /dev/null" >> ~/.ssh/config
  echo "  ProxyJump azure-bastion-preprod" >> ~/.ssh/config
  echo "  StrictHostKeyChecking no" >> ~/.ssh/config
  echo "Host azure-bastion-preprod" >> ~/.ssh/config
  echo "  User $BASTION_USER@azure.bukalapak.io" >> ~/.ssh/config
  echo "  CertificateFile ~/.ssh/id_rsa.pub-aadcert.pub" >> ~/.ssh/config
  echo "  Hostname 172.27.16.7" >> ~/.ssh/config
  echo "  StrictHostKeyChecking no" >> ~/.ssh/config
}
function append_prod()
{
  echo ~/.ssh/config
  echo "### AZURE VM" >> ~/.ssh/config
  echo "Host 172.*" >> ~/.ssh/config
  echo "  User $BASTION_USER@azure.bukalapak.io" >> ~/.ssh/config
  echo "  CertificateFile ~/.ssh/id_rsa.pub-aadcert.pub" >> ~/.ssh/config
  echo "  UserKnownHostsFile /dev/null" >> ~/.ssh/config
  echo "  ProxyJump azure-bastion-prod" >> ~/.ssh/config
  echo "  StrictHostKeyChecking no" >> ~/.ssh/config
  echo "Host azure-bastion-prod" >> ~/.ssh/config
  echo "  User $BASTION_USER@azure.bukalapak.io" >> ~/.ssh/config
  echo "  CertificateFile ~/.ssh/id_rsa.pub-aadcert.pub" >> ~/.ssh/config
  echo "  Hostname 172.25.16.5" >> ~/.ssh/config
  echo "  StrictHostKeyChecking no" >> ~/.ssh/config
}
if [ -f "$filename" ]; then
  echo "File exists"
  echo "Creating (append) ssh config . . ."
  if [ $ENV == "prod" ]; then
    echo "Append prod"
    append_prod
  else
    echo "Append preprod"
    append_prep
  fi
  echo "Creating ssh config finished . . ."
else
  echo "File does not exist. Creating file . . ."
  touch ~/.ssh/config
  echo "Creating (append) ssh config . . ."
  if [ $ENV == "prod" ]; then
    echo "Append prod"
    append_prod
  else
    echo "Append preprod"
    append_prep
  fi
  echo "Creating ssh config finished . . ."
fi

import yaml, json, os
import subprocess

# Require env variable

# Get Env Variable & Fullpath file
env = os.getenv('ENV')
msvc = os.getenv('MICROSERVICE_NAME')
clusterfilename = os.getenv('CLUSTERNAME')
fullpathmetadata = clusterfilename.replace("-","_")
filename = "services/{}/ansible/inventory/".format(msvc) + "{}/group_vars/".format(env) + "metadata_kafka_cluster_kafka_{}.yaml".format(fullpathmetadata)
print(filename)

# Get yaml value
with open(filename) as stream:
  data = yaml.load(stream ,Loader=yaml.BaseLoader)

# Get zookeeper host & redirect to ZOOKEEPER_HOST
ZOOKEEPER_HOST = data['zookeeper_connect']

def get_existing_topic(zookeeper_host):
  bashCMD=["kafka/bin/kafka-topics.sh","--list","--zookeeper", "{}".format(zookeeper_host)]
  process = subprocess.Popen(bashCMD, stdout=subprocess.PIPE)
  output, error = process.communicate()
  return output.decode("utf-8").splitlines()

exist_topics = get_existing_topic(ZOOKEEPER_HOST)
new_topics = data['kafka_topic_config']

# remove exist topic from new topics
need_to_create_topic = [d for d in new_topics if d['topic'] not in exist_topics]

# Get rf & pn then execute create topic on looping
for i in need_to_create_topic:
    TOPIC_NAME = i['topic']
    REPLICATION_FACTOR = i['replication_factor']
    PARTITION_NUMBER = i['partition_number']
    bashCMD=["kafka/bin/kafka-topics.sh","--zookeeper", "{}".format(ZOOKEEPER_HOST),"--create","--topic","{}".format(TOPIC_NAME), "--replication-factor", "{}".format(REPLICATION_FACTOR), "--partitions", "{}".format(PARTITION_NUMBER)]
    process = subprocess.Popen(bashCMD, stdout=subprocess.PIPE)
    output, error = process.communicate()
    print(output.decode("utf-8"))

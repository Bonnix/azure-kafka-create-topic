#!/bin/bash

resource_group=qsh

for i in {0..2}; do
 az vm create \
 --resource-group $resource_group \
 --name kafka-qsh-staging-$i \
 --image Debian:debian-10:10-cloudinit-gen2:0.0.999	 \
 --admin-username syseng \
 --ssh-key-values ~/.ssh/id_rsa.pub \
 --size Standard_B1S

 az vm disk attach \
 -g $resource_group \
 --vm-name kafka-qsh-staging-$i \
 --name kafka-qsh-staging-$i-data \
 --new \
 --size-gb 10

done


Role Name
=========

Role to install java package, default is to install openjdk version instead of Oracle.

Requirements
------------

No requirements

Role Variables
--------------

Default variables:
```
java_openjdk_ver: declare the version of openjdk-<version>-jre-headless package, default is 8
java_use_openjdk: declare if we want to use openjdk package, default is yes

java_use_repo: declare if we want to use our own repository URL for Oracle version of java package, default is yes

java_repo_url: declare our own repository URL, default is 'https://s3-ap-southeast-1.amazonaws.com/bl-sources/pub/java/oracle'
java_repo_pkg: declare Oracle java package name in our own repository, default is 'server-jre-8u112-linux-x64.tar.gz'
java_repo_ver: declare Oracle java version in our own repository, default is '1.8.0_112'

java_url: declare Oracle java repository URL, default is 'http://download.oracle.com/otn-pub/java/jdk/8u192-b12/750e1c8617c5452694857ad95c3ee230'
java_pkg: declare Oracle java package name, default is 'server-jre-8u192-linux-x64.tar.gz'
java_ver: declare Oracle java version, default is '1.8.0_192'
```

Dependencies
------------

No dependencies

Example Playbook
----------------

Example playbook
```
- hosts: "{{ target_hosts | default('all') }}
  vars_files:
    - inventory/group_vars/java
  roles:
    - java
```

License
-------

BSD

Author Information
------------------

Bukalapak System Engineer

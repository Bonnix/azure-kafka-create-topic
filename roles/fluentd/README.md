# Fluentd Ansible Role

## Summary
Ansible role for installing Fluentd to VM

## Requirements
- ansible 2.10

## Supported Linux Distributions
- Debian stretch, buster

## Role Variables
- `env`: Name of environment. e.g. preproduction, production.
- `fluentd_service`: Service whose logs will be read by fluentd e.g. elasticsearch, kafka, mysql. Refer to vars folder for list of supported services.
- `microservice_name`: Name of microservice e.g. aleppo, goods, mothership.

## Importing with Ansible Galaxy
This role can be imported using ansible galaxy
```sh
ansible-galaxy install git+git@git.gitlab.cloud.bukalapak.io:ansible/roles/fluentd.git
```

## Example Playbook
```yaml
- name: Deploy Fluentd
  hosts: '{{ group_name|default(deploy_hosts) }}'
  become: yes

  vars:
    env: production
    fluentd_service: mysql
    microservice_name: mothership

  roles:
    - fluentd
```

## Author
infra-log

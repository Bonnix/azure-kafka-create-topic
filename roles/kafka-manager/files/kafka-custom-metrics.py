import json
import requests
from datadog_checks.checks import AgentCheck

class kafka_custom_metrics(AgentCheck):

  def check(self, instance):
    url_cluster = 'http://localhost:9000/api/status/clusters'
    reponse_cluster = requests.get(url_cluster)
    cluster_data = reponse_cluster.json()
    for cluster in cluster_data['clusters']['active']:
      if cluster['enabled'] == True:
        cluster_name=cluster['name']
        url_topic = 'http://localhost:9000/api/status/' + cluster_name + '/topicIdentities'
        response_topic = requests.get(url_topic)
        if response_topic.status_code == 200:
          topic_data = response_topic.json()
          self.broker_skew(topic_data, cluster_name)
          self.leader_skew(topic_data, cluster_name)
          self.broker_spread(topic_data, cluster_name)

  def broker_skew(self, topic_data, cluster_name):
    broker_skew_count = 0
    skewed_topics = []
    for topicIdentities in topic_data['topicIdentities']:
      topic = topicIdentities['topic']
      broker_skew = topicIdentities['brokersSkewPercentage']
      if broker_skew > 0:
        broker_skew_count = broker_skew_count + 1
        skewed_topics.append(topic)

    tags = []
    tagraw_0 = 'cluster:' + cluster_name
    tags.append(tagraw_0)

    if len(skewed_topics) == 0 :
      tags.append('topic:none')
    else:
      for skewed_topic in skewed_topics:
        tags.append('topic:' + skewed_topic)

    self.gauge('kafka.brokerSkew', broker_skew_count, tags)

  def leader_skew(self, topic_data, cluster_name):
    leader_skew_count = 0
    skewed_leader_topics = []
    for topicIdentities in topic_data['topicIdentities']:
      topic = topicIdentities['topic']
      for partitionByBroker in topicIdentities['partitionsByBroker']:
        if partitionByBroker['isLeaderSkewed'] == True :
          leader_skew_count = leader_skew_count + 1
          skewed_leader_topics.append(topic)

    tags = []
    tagraw_0 = 'cluster:' + cluster_name
    tags.append(tagraw_0)
    if len(skewed_leader_topics) == 0 :
      tags.append('topic:none')
    else:
      for skewed_leader_topic in skewed_leader_topics:
        tags.append('topic:' + skewed_leader_topic)

    self.gauge('kafka.leaderSkew', leader_skew_count, tags)
  
  def broker_spread(self, topic_data, cluster_name):
    broker_spread_count = 0
    spread_topics = []
    for topicIdentities in topic_data['topicIdentities']:
      topic = topicIdentities['topic']
      broker_spread = topicIdentities['brokersSpreadPercentage']
      if broker_spread < 100:
        broker_spread_count = broker_spread_count + 1
        spread_topics.append(topic)

    tags = []
    tagraw_0 = 'cluster:' + cluster_name
    tags.append(tagraw_0)

    if len(spread_topics) == 0 :
      tags.append('topic:none')
    else:
      for spread_topic in spread_topics:
        tags.append('topic:' + spread_topic)

    self.gauge('kafka.brokerSpread', broker_spread_count, tags)

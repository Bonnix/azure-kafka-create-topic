import json
import requests
import re
from datadog_checks.checks import AgentCheck

class kafka_manager_capacity_metrics(AgentCheck):

  def check(self, instance):
    url_cluster = 'http://localhost:9000/api/status/clusters'
    response_cluster = requests.get(url_cluster)
    cluster_data = response_cluster.json()
    all_topics_list = []
    for cluster in cluster_data['clusters']['active']:
      if cluster['enabled'] == True:
        cluster_name = cluster['name']
        url_topic = 'http://localhost:9000/api/status/' + cluster_name + '/topics'
        response_topic = requests.get(url_topic)
        if response_topic.status_code == 200:
          topic_data = response_topic.json()
          all_topics_list.append(len(topic_data['topics'])-1)
          all_topics_all_cluster = sum(all_topics_list)
          self.kafka_manager_topics_count(topic_data, all_topics_all_cluster, cluster_name)

  def kafka_manager_topics_count(self, topic_data, all_topics_all_cluster, cluster_name):
    topics_list = topic_data['topics'][1:]
    unique_topic_string = []
    for topics in topics_list:
      split_topic_string = topics.split('-')
      join_topic_string = '-'.join(split_topic_string[0:2])
      if join_topic_string not in unique_topic_string:
        unique_topic_string.append(join_topic_string)
    for query_topic in unique_topic_string:
      topic_search = re.compile("^"+query_topic+"-(?:q|e)-t-.*$")
      tribe_topics = list(filter(topic_search.match, topics_list))
      topics_count = len(tribe_topics)/all_topics_all_cluster*100
      tags = []
      tagraw_0 = 'tribe_name:' + query_topic.split('-')[0]
      tagraw_1 = 'service_name:' + query_topic.split('-')[1]
      tagraw_2 = 'cluster_name:' + cluster_name
      tags.append(tagraw_0)
      tags.append(tagraw_1)
      tags.append(tagraw_2)
  
      self.gauge('kafka.ManagerTopics', topics_count, tags)

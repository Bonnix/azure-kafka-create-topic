import json
import requests
from datadog_checks.checks import AgentCheck

class md_stat_checker(AgentCheck):

    def check(self, instance):

      url_cluster = 'http://localhost:9000/api/status/clusters'
      reponse_cluster = requests.get(url_cluster)
      cluster_data = reponse_cluster.json()

      for cluster in cluster_data['clusters']['active']:
        cluster_name=cluster['name']
        url_topic = 'http://localhost:9000/api/status/' + cluster_name + '/topicIdentities'
        response_topic = requests.get(url_topic)
        if response_topic.status_code == 200:
          topic_data = response_topic.json()
          broker_skew_count = 0
          skewed_topics = []
          for topicIdentities in topic_data['topicIdentities']:
            topic = topicIdentities['topic']
            broker_skew = topicIdentities['brokersSkewPercentage']
            if broker_skew > 0:
              broker_skew_count = broker_skew_count + 1
              skewed_topics.append(topic)
          
          tags = []
          tagraw_0 = 'cluster:' + cluster_name
          tags.append(tagraw_0)

          if len(skewed_topics) == 0 :
            tags.append('topic:none')
          else:
            for skewed_topic in skewed_topics:
              tags.append('topic:' + skewed_topic)
          
          self.gauge('kafka.brokerSkew', broker_skew_count, tags)

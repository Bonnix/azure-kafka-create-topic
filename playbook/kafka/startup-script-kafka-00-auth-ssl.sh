#!/bin/bash

HOSTNAME="$(hostname -f)"

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root" >&2
	exit 1
fi

if grep -q REPLACEKAFKAID /opt/kafka/config/server.properties; then
  kafka_id="{{ kafka_id }}"
  echo "[init] Got kafka_id ${kafka_id}"

  kafka_ip="{{ kafka_ip }}"
  echo "[init] Got kafka_IP Address ${kafka_ip}"

  kafka_zone="{{ kafka_zone }}"
  echo "[init] Got kafka_zone ${kafka_zone}"

  echo "[init] Modifiying kafka.server.properties"
  sed -i \
    -e "s|^broker.id=.*|broker.id=${kafka_id}|" \
    -e "s|^listeners=.*|listeners=PLAINTEXT://${kafka_ip}:9092,SSL://${kafka_ip}:9093|" \
    -e "s|^advertised.listeners=.*|advertised.listeners=PLAINTEXT://${kafka_ip}:9092,SSL://${HOSTNAME}:9093|" \
    -e "s|^broker.rack=.*|broker.rack=${kafka_zone}|" \
    /opt/kafka/config/server.properties
else
  echo "[init] kafka.server.properties already modified"
fi

if grep -q REPLACEME /lib/systemd/system/kafka.service; then
  kafka_ip="{{ kafka_ip }}"
  echo "[init] Got kafka_IP Address ${kafka_ip}"

  echo "[init] Modifiying kafka.service"
  sed -i \
    -e "s|REPLACEME|${kafka_ip}|" \
    /lib/systemd/system/kafka.service
else
  echo "[init] kafka.service already modified"
fi

bash /etc/profile.d/java-jdk.sh

echo "[init] systemctl daemon-reload"
systemctl daemon-reload

echo "[init] Initializing kafka..."

filesystem='{{ kafka_filesystem }}'
# setup data dir
## reboot scenario: do nothing
# setup data dir
## reboot scenario: do nothing

mkdir -p /data/kafka

count=-2
lsblk -d | grep disk | cut -d" " -f1 | 
while read -r line
do
   let "count += 1"
   if [[ "$line" != "sda" ]] && [[ "$line" != "sdb" ]]
   then
     disk="/dev/$line"
     mountpoint="/data/kafka"
     mkdir -p /data/kafka

     if grep -q ${mountpoint} /proc/mounts; then
      echo "[init] ${mountpoint} already mounted"
     ## restack scenario: mount data disk, setup fstab
     elif lsblk -f ${disk} | grep -q ${filesystem}; then
       echo "[init] ${disk} already formatted, going to mount directly"
       mount -o discard,noatime ${disk} ${mountpoint}
       echo "${disk} ${mountpoint} ${filesystem} discard,noatime 0 0" >> /etc/fstab
     # first boot scenario: format data disk, mount data disk, setup fstab, chown mountpoint
     else
       echo "[init] Using filesystem: ${filesystem}"

       if find ${mountpoint} -mindepth 1 -print -quit | grep -q .; then
         echo "[init] ${mountpoint} is not mounted but not empty" >&2
         exit 1
       fi
       mkfs.${filesystem} ${disk} || echo "[init] Attempt to mount anyway"

       mount -o discard,noatime ${disk} ${mountpoint}
       echo "$(blkid ${disk} | cut -d" " -f2) ${mountpoint} ${filesystem} discard,noatime 0 0" >> /etc/fstab
     fi
   fi
done

echo "[init] Starting kafka..."
service kafka stop
service kafka start

# Check SSL Directory 
if [ -d "/data/kafka/ssl/" ]; then
    echo "Directory /data/kafka/ssl/ exists." 
    echo "Copy certificate from /data/kafka/ssl to /opt/kafka/ssl for consistency."
    cp -p /data/kafka/ssl/ca-{key,cert} /opt/kafka/ssl/

else
    echo "Create directory /data/kafka/ssl."
    mkdir /data/kafka/ssl
    echo "Copy certificate from /opt/kafka/ssl to /data/kafka/ssl."
    cp -p /opt/kafka/ssl/ca-{key,cert} /data/kafka/ssl/
fi 
# Configure SSL
SRVPASS=${server_secret}
echo "[init] Got server_secret"
SSL_DIR="/data/kafka/ssl"
KAFKA_SERVER_KEYSTORE="${SSL_DIR}/kafka.server.keystore.jks"
KAFKA_SERVER_TRUSTORE="${SSL_DIR}/kafka.server.truststore.jks"

## create a server certificate !!

if [ -f ${KAFKA_SERVER_KEYSTORE} ]; then
  echo "File ${KAFKA_SERVER_KEYSTORE} exists."
else
  echo "Create a server keystore."
  keytool -genkey -keystore ${KAFKA_SERVER_KEYSTORE} -validity 3650 -storepass ${SRVPASS} -keypass ${SRVPASS}  -dname "CN=${HOSTNAME}" -storetype pkcs12
  ## create a certification request file, to be signed by the CA
  echo "Create a certification request file, to be signed by the CA."
  keytool -keystore ${KAFKA_SERVER_KEYSTORE} -certreq -file ${SSL_DIR}/cert-file -storepass ${SRVPASS} -keypass ${SRVPASS}
  ## sign the server certificate => output: file "cert-signed"
  echo "Sign the server certificate."
  openssl x509 -req -CA ${SSL_DIR}/ca-cert -CAkey ${SSL_DIR}/ca-key -in ${SSL_DIR}/cert-file -out ${SSL_DIR}/cert-signed -days 3650 -CAcreateserial -passin pass:${SRVPASS}
fi

if [ -f ${KAFKA_SERVER_TRUSTORE} ]; then
  echo "File ${KAFKA_SERVER_TRUSTORE} exists."
else
  # Trust the CA by creating a truststore and importing the ca-cert
  echo "Trust the CA by creating a truststore and importing the ca-cert."
  keytool -keystore ${KAFKA_SERVER_TRUSTORE} -alias CARoot -import -file ${SSL_DIR}/ca-cert -storepass ${SRVPASS} -keypass ${SRVPASS} -noprompt
  # Import CA and the signed server certificate into the keystore
  echo "Import CA and the signed server certificate into the keystore."
  keytool -keystore ${KAFKA_SERVER_KEYSTORE} -alias CARoot -import -file ${SSL_DIR}/ca-cert -storepass ${SRVPASS} -keypass ${SRVPASS} -noprompt
  keytool -keystore ${KAFKA_SERVER_KEYSTORE} -import -file ${SSL_DIR}/cert-signed -storepass ${SRVPASS} -keypass ${SRVPASS} -noprompt
fi

# Create SSL for Client
CLIPASS=${client_secret}
echo "[init] Got client_secret"
RAW_CLI_HOSTNAME=${client_hostname}
echo "[init] Got client_hostname ${RAW_CLI_HOSTNAME}"

for CLI_HOSTNAME in $(echo ${RAW_CLI_HOSTNAME} | sed -e $'s/,/\\\n/g'); do
  echo "Create certificate for ${CLI_HOSTNAME}."
  CLI_SSL_DIR="${SSL_DIR}/client/${CLI_HOSTNAME}"
  KAFKA_CLIENT_KEYSTORE="${CLI_SSL_DIR}/kafka.client.keystore.jks"
  KAFKA_CLIENT_TRUSTORE="${CLI_SSL_DIR}/kafka.client.truststore.jks"

  ## create a CLIENT certificate

  if [ -f ${KAFKA_CLIENT_KEYSTORE} ]; then
    echo "File ${KAFKA_CLIENT_KEYSTORE} exists."
  else
    mkdir -p ${CLI_SSL_DIR}
    echo "Create a client keystore."
    keytool -genkey -keystore ${KAFKA_CLIENT_KEYSTORE} -validity 3650 -storepass $CLIPASS -keypass $CLIPASS  -dname "CN=${CLI_HOSTNAME}" -alias ${CLI_HOSTNAME} -storetype pkcs12
    ## create a certification request file, to be signed by the CA
    echo "Create a certification request file, to be signed by the CA."
    keytool -keystore ${KAFKA_CLIENT_KEYSTORE} -certreq -file ${CLI_SSL_DIR}/client-cert-sign-request -alias ${CLI_HOSTNAME} -storepass $CLIPASS -keypass $CLIPASS
    ## sign the client certificate
    echo "Sign the client certificate."
    openssl x509 -req -CA ${SSL_DIR}/ca-cert -CAkey ${SSL_DIR}/ca-key -in ${CLI_SSL_DIR}/client-cert-sign-request -out ${CLI_SSL_DIR}/client-cert-signed -days 3650 -CAcreateserial -passin pass:$SRVPASS
    echo "Import CA and the signed client certificate into the keystore."
    keytool -keystore ${KAFKA_CLIENT_KEYSTORE} -alias CARoot -import -file ${SSL_DIR}/ca-cert -storepass $CLIPASS -keypass $CLIPASS -noprompt
    keytool -keystore ${KAFKA_CLIENT_KEYSTORE} -import -file ${CLI_SSL_DIR}/client-cert-signed -alias ${CLI_HOSTNAME}  -storepass $CLIPASS -keypass $CLIPASS -noprompt
    echo "Create client truststore."
    keytool -keystore ${KAFKA_CLIENT_TRUSTORE} -alias CARoot -import -file ${SSL_DIR}/ca-cert  -storepass $CLIPASS -keypass $CLIPASS -noprompt
  fi
done
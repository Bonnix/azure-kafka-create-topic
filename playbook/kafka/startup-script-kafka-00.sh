#!/bin/bash

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root" >&2
	exit 1
fi

if grep -q REPLACEKAFKAID /opt/kafka/config/server.properties; then
  kafka_id="{{ kafka_id }}"
  echo "[init] Got kafka_id ${kafka_id}"

  kafka_ip="{{ kafka_ip }}"
  echo "[init] Got kafka_IP Address ${kafka_ip}"

  kafka_zone="{{ kafka_zone }}"
  echo "[init] Got kafka_zone ${kafka_zone}"

  echo "[init] Modifiying kafka.server.properties"
  sed -i \
    -e "s|^broker.id=.*|broker.id=${kafka_id}|" \
    -e "s|^listeners=.*|listeners=PLAINTEXT://${kafka_ip}:9092|" \
    -e "s|^advertised.listeners=.*|advertised.listeners=PLAINTEXT://${kafka_ip}:9092|" \
    -e "s|^broker.rack=.*|broker.rack=${kafka_zone}|" \
    /opt/kafka/config/server.properties
else
  echo "[init] kafka.server.properties already modified"
fi

if grep -q REPLACEME /lib/systemd/system/kafka.service; then
  kafka_ip="{{ kafka_ip }}"
  echo "[init] Got kafka_IP Address ${kafka_ip}"

  echo "[init] Modifiying kafka.service"
  sed -i \
    -e "s|REPLACEME|${kafka_ip}|" \
    /lib/systemd/system/kafka.service
else
  echo "[init] kafka.service already modified"
fi

bash /etc/profile.d/java-jdk.sh

echo "[init] systemctl daemon-reload"
systemctl daemon-reload

echo "[init] Initializing kafka..."

filesystem='{{ kafka_filesystem }}'
# setup data dir
## reboot scenario: do nothing
# setup data dir
## reboot scenario: do nothing

mkdir -p /data/kafka

count=-2
lsblk -d | grep disk | cut -d" " -f1 | 
while read -r line
do
   let "count += 1"
   if [[ "$line" != "sda" ]] && [[ "$line" != "sdb" ]]
   then
     disk="/dev/$line"
     mkdir 
     mountpoint="/data/kafka/data$count"
     mkdir -p /data/kafka/data$count

      if grep -q ${mountpoint} /proc/mounts; then
       echo "[init] ${mountpoint} already mounted"
      ## restack scenario: mount data disk, setup fstab
      elif lsblk -f ${disk} | grep -q ${filesystem}; then
       echo "[init] ${disk} already formatted, going to mount directly"
       mount -o discard,noatime ${disk} ${mountpoint}
       echo "${disk} ${mountpoint} ${filesystem} discard,noatime 0 0" >> /etc/fstab
     # first boot scenario: format data disk, mount data disk, setup fstab, chown mountpoint
      else
        echo "[init] Using filesystem: ${filesystem}"

        if find ${mountpoint} -mindepth 1 -print -quit | grep -q .; then
          echo "[init] ${mountpoint} is not mounted but not empty" >&2
          exit 1
        fi
        mkfs.${filesystem} ${disk} || echo "[init] Attempt to mount anyway"

        mount -o discard,noatime ${disk} ${mountpoint}
        echo "$(blkid ${disk} | cut -d" " -f2) ${mountpoint} ${filesystem} discard,noatime 0 0" >> /etc/fstab
      fi
   fi
done

echo "[init] Starting kafka..."
service kafka stop
service kafka start
#!/bin/bash

#set -e

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root" >&2
	exit 1
fi

filesystem=xfs
# setup data dir
## reboot scenario: do nothing
# setup data dir
## reboot scenario: do nothing

# mkdir -p /data/kafka

count=-2
lsblk -d | grep disk | cut -d" " -f1 | 
while read -r line
do
   let "count += 1"
   if [[ "$line" != "sda" ]] && [[ "$line" != "sdb" ]]
   then
     disk="/dev/$line"
     mountpoint="/data"
     mkdir -p /data

     if grep -q ${mountpoint} /proc/mounts; then
      echo "[init] ${mountpoint} already mounted"
     ## restack scenario: mount data disk, setup fstab
     elif lsblk -f ${disk} | grep -q ${filesystem}; then
       echo "[init] ${disk} already formatted, going to mount directly"
       mount -o discard,noatime ${disk} ${mountpoint}
       echo "${disk} ${mountpoint} ${filesystem} discard,noatime 0 0" >> /etc/fstab
     # first boot scenario: format data disk, mount data disk, setup fstab, chown mountpoint
     else
       echo "[init] Using filesystem: ${filesystem}"

       if find ${mountpoint} -mindepth 1 -print -quit | grep -q .; then
         echo "[init] ${mountpoint} is not mounted but not empty" >&2
         exit 1
       fi
       mkfs.${filesystem} ${disk} || echo "[init] Attempt to mount anyway"

       mount -o discard,noatime ${disk} ${mountpoint}
       echo "$(blkid ${disk} | cut -d" " -f2) ${mountpoint} ${filesystem} discard,noatime 0 0" >> /etc/fstab
     fi
   fi
done

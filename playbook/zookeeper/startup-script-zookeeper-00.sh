#!/bin/bash

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root" >&2
	exit 1
fi

if grep -q REPLACEME /lib/systemd/system/zookeeper.service; then
  zookeeper_ip="{{ zookeeper_ip }}"
  echo "[init] Got zookeeper_IP Address ${zookeeper_ip}"

  echo "[init] Modifiying zookeeper.service"
  sed -i \
    -e "s|REPLACEME|${zookeeper_ip}|" \
    /lib/systemd/system/zookeeper.service
else
  echo "[init] zookeeper.service already modified"
fi

bash /etc/profile.d/java-jdk.sh

echo "[init] systemctl daemon-reload"
systemctl daemon-reload

echo "[init] Initializing zookeeper..."


filesystem='{{ zookeeper_filesystem }}'
echo $filesystem
# setup data dir
## reboot scenario: do nothing
# setup data dir
## reboot scenario: do nothing

mkdir -p /data/zookeeper

count=-2
lsblk -d | grep disk | cut -d" " -f1 | 
while read -r line
do
   let "count += 1"
   if [[ "$line" != "sda" ]] && [[ "$line" != "sdb" ]]
   then
     disk="/dev/$line"
     mountpoint="/data/zookeeper/data$count"
     mkdir -p /data/zookeeper/data$count

     if grep -q ${mountpoint} /proc/mounts; then
      echo "[init] ${mountpoint} already mounted"
     ## restack scenario: mount data disk, setup fstab
     elif lsblk -f ${disk} | grep -q ${filesystem}; then
       echo "[init] ${disk} already formatted, going to mount directly"
       mount -o discard,noatime ${disk} ${mountpoint}
       echo "${disk} ${mountpoint} ${filesystem} discard,noatime 0 0" >> /etc/fstab
     # first boot scenario: format data disk, mount data disk, setup fstab, chown mountpoint
     else
       echo "[init] Using filesystem: ${filesystem}"

       if find ${mountpoint} -mindepth 1 -print -quit | grep -q .; then
         echo "[init] ${mountpoint} is not mounted but not empty" >&2
         exit 1
       fi
       mkfs.${filesystem} ${disk} || echo "[init] Attempt to mount anyway"

       mount -o discard,noatime ${disk} ${mountpoint}
       echo "$(blkid ${disk} | cut -d" " -f2) ${mountpoint} ${filesystem} discard,noatime 0 0" >> /etc/fstab
     fi
   fi
done

if [ -f "/data/zookeeper/myid" ]; then
  echo "myid file found, do nothing"
else
  echo "writing myid file"
  echo '{{ zookeeper_myid }}' > /data/zookeeper/myid
fi

echo "[init] Stopping zookeeper..."
service zookeeper stop
